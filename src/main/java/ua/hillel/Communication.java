package ua.hillel;

import java.util.Scanner;

public class Communication {

    void greeting() {
        System.out.println("Welcome to our Game!");
    }

    public int getNumberOfPlayers() {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Please, enter the number of players below: ");
        int number = myObj.nextInt();
        return number;
    }

    public Player[] getNamesOfPlayers() {
        Scanner myObj = new Scanner(System.in);
        int number = getNumberOfPlayers();
        System.out.println("Please, enter names of players below: ");
        Player[] players = new Player[number];
        for (int i = 0; i < number; i++) {
            System.out.println("Player #" + (i + 1));
            String name = myObj.next();
            players[i] = new Player(name);
        }
        return players;
    }
}
