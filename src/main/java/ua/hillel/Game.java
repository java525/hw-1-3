package ua.hillel;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Game {

    public void startGame() {
        Dice myDice = new Dice(6);
        Communication myCommunication = new Communication();
        myCommunication.greeting();

        Player[] players = myCommunication.getNamesOfPlayers();

        Map<Player, Integer> rollings = new HashMap<>();
        playersRollDice(players, myDice, rollings);
        printRollResult(rollings);
        defineWinner(rollings);

        repeatGame();
    }

    private void playersRollDice(Player[] players, Dice myDice, Map<Player, Integer> rollings) {
        for (int i = 0; i < players.length; i++) {
            System.out.println("Player # " + players[i].getName() + " is rolling a dice:");
            int result = myDice.rollDice();
            rollings.put(players[i], result);
            System.out.println(result);
        }
    }

    private void printRollResult(Map<Player, Integer> rollings) {
        System.out.println("SCORES: ");
        for (Map.Entry<Player, Integer> entry : rollings.entrySet()) {
            System.out.println(entry.getKey().getName() + " : " + entry.getValue());
        }
    }

    private void defineWinner(Map<Player, Integer> rollings) {
        int max = 0;
        for (Map.Entry<Player, Integer> entry : rollings.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
            }
        }
        System.out.println("MAXIMUM SCORE: " + max);

        System.out.println("WINNERS: ");
        for (Map.Entry<Player, Integer> entry : rollings.entrySet()) {
            if (entry.getValue() == max) {
                System.out.println(entry.getKey().getName());
            }
        }
    }

    private void repeatGame() {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Do you want to repeat the game? Type Y to continue.");
        String answer = myObj.next();
        while (answer.equals("y") || answer.equals("Y")) {
            startGame();
        }
    }
}
