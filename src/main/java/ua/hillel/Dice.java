package ua.hillel;

public class Dice {

    private int numberOfSides;

    public Dice(int numberOfSides) {
        this.numberOfSides = numberOfSides;
    }

    public int rollDice() {
        int result = (int) (Math.random() * (numberOfSides) + 1);
        return result;
    }
}
