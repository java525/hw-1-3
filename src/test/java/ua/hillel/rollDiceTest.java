package ua.hillel;

import org.junit.jupiter.api.Test;

class rollDiceTest {

    public static final int numberOfSides = 6;

    @Test
    void IsRollingDiceResultEqualsOrLess() {

        Dice testDice = new Dice(numberOfSides);
        int result = testDice.rollDice();

        assert result > 0 && result <= numberOfSides;
    }
}
