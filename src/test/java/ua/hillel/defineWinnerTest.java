package ua.hillel;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class defineWinnerTest {

    Map<Player, Integer> createTestMap() {
        Player player1 = new Player("Player 1");
        Player player2 = new Player("Player 2");
        Player player3 = new Player("Player 3");
        Player player4 = new Player("Player 4");
        Player player5 = new Player("Player 5");
        Player player6 = new Player("Player 6");

        Map<Player, Integer> rollingResults = new HashMap<>();

        rollingResults.put(player1, 1);
        rollingResults.put(player2, 3);
        rollingResults.put(player3, 5);
        rollingResults.put(player4, 5);
        rollingResults.put(player5, 2);
        rollingResults.put(player6, 5);

        return rollingResults;
    }

    @Test
    void isMaxScoreCorrectlyDefined() {

        Map<Player, Integer> rollingResults;
        rollingResults = createTestMap();

        int max = 0;
        for (Map.Entry<Player, Integer> entry : rollingResults.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
            }
        }
        assertEquals(max, 5);
    }

    @Test
    void isWinnerChosenCorrectly() {

        Map<Player, Integer> rollingResults;
        rollingResults = createTestMap();
        int maxValue = 5;

        Set<String> names = new HashSet<String>();

        for (Map.Entry<Player, Integer> entry : rollingResults.entrySet()) {
            if (entry.getValue() == maxValue) {
                names.add(entry.getKey().getName());
            }
        }
        assert names.contains("Player 3");
        assert names.contains("Player 4");
        assert names.contains("Player 6");
    }
}

